const miniCss = require('mini-css-extract-plugin');
const optimizeCss = require("css-minimizer-webpack-plugin");
const terser = require('terser-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
    entry: './src/js/app.js',
    mode: 'production',
    output: {
        path: `${__dirname}/dist`,
        filename: 'bundle.js',
    },
    plugins: [
        new miniCss(),
    ],
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    miniCss.loader,
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test:/\.(svg|gif|png|eot|woff|woff2|ttf)$/,
                use: [
                    'url-loader',
                ],
            }
        ],
    },
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                extractComments: false,
            }),
            new optimizeCss(),
        ]
    }
}