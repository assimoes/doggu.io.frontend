from node:latest AS build

WORKDIR /build
COPY . /build
RUN yarn install
RUN yarn run build

from nginx
COPY --from=build /build/dist /usr/share/nginx/doggu.io/dist
COPY --from=build /build/index.html /usr/share/nginx/doggu.io
COPY --from=build /build/img /usr/share/nginx/doggu.io/img
COPY ./nginx/public.conf /etc/nginx/conf.d/default.conf